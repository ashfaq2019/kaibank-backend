package com.kaibank.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KaibankSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(KaibankSystemApplication.class, args);
	}

}
