package com.kaibank.system.repository;

import com.kaibank.system.entity.Branch;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This BranchRepository class is used to handle database transactions related to branch.
 *
 * @author Pabasara
 * @version 1.0
 * @since 1.0
 */
public interface BranchRepository extends JpaRepository<Branch, Long> {}
